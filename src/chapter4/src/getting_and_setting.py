# USAGE
# python src/chapter4/src/getting_and_setting.py --image images/trex.png



# Import the necessary packages
from __future__ import print_function
import argparse
import random
import cv2

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="Path to the image")
args = vars(ap.parse_args())

print(args);

# Load the image and show some basic information on it
image = cv2.imread(args["image"])
# print("width: {} pixels".format(image.shape[1]))
# print("height: {} pixels".format(image.shape[0]))
# print("channels: {}".format(image.shape[2]))

# Show the image and wait for a keypress
cv2.imshow("Original Image", image)
cv2.waitKey(0)

(b, g, r) = image[219, 90]
print("Pixel at (90,219) - Red: {}, Green: {}, Blue: {}".format(r, g, b))

image[0, 0] = (0, 0, 255)
(b, g, r) = image[0, 0]
print("Pixel at (0,0) - Red: {}, Green: {}, Blue: {}".format(r, g, b))


cv2.imshow("Corner Image", image[0:100, 0:100])

(b, g, r) = image[0, 0]
print("Pixel at (0,0) - Red: {}, Green: {}, Blue: {}".format(r, g, b))

image[0:100, 0:100] = (0, 255, 0)
(b, g, r) = image[0, 0]
print("Pixel at (0,0) - Red: {}, Green: {}, Blue: {}".format(r, g, b))


cv2.imshow("Updated Image", image)



margin = 5
for i in range(0, 50):
    # Pick random pixel
    randint2 = random.randint(margin, image.shape[1] - (margin + 1))
    randint3 = random.randint(margin, image.shape[0] - (margin + 1))
    (x, y) = (randint2, randint3)

    # Set random color
    image[y-margin:y+margin, x-margin:x+margin] = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

cv2.imshow("Manipulated Image", image)
cv2.waitKey(0)
