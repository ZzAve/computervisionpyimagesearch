from __future__ import print_function
import image_loader
import numpy as np
import cv2
from matplotlib import pyplot as plt

image = image_loader.load_image_from_args()

# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# cv2.imshow("Gray", gray)


plt.figure()
plt.title("'Flattened' color historgram")
plt.xlabel("Pixel value")
plt.ylabel("Occurences")

channels = cv2.split(image)
colors = ("b", "g", "r")
for (channel, color) in zip(channels, colors):
    hist = cv2.calcHist([channel], [0], None, [256], [0, 256])
    plt.plot(hist, color=color)
    plt.xlim([0, 256])

plt.show()

# Sub plots
fig = plt.figure()

# Combining G and B
ax = fig.add_subplot(131)
hist = cv2.calcHist([channels[1], channels[0]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation="nearest")
ax.set_title("2D color histogram for Green AND Blue")
plt.colorbar(p)

# Combining G and R
ax = fig.add_subplot(132)
hist = cv2.calcHist([channels[1], channels[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation="nearest")
ax.set_title("2D color histogram for Green AND Red")
plt.colorbar(p)

# Combining B and R
ax = fig.add_subplot(133)
hist = cv2.calcHist([channels[0], channels[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
p = ax.imshow(hist, interpolation="nearest")
ax.set_title("2D color histogram for Blue AND Red")
plt.colorbar(p)

# Showing
print("2D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))
plt.show()
# cv2.waitKey()


hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
# plt.plot(hist)
plt.show()
print("3D histogram shape: {}, with {} values".format(hist.shape, hist.flatten().shape[0]))
