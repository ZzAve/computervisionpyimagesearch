from __future__ import print_function
import image_loader
import numpy as np
import cv2
from matplotlib import pyplot as plt

image = image_loader.load_image_from_args()

image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
eq = cv2.equalizeHist(image)

cv2.imshow("img (gray / equalized)", np.hstack([image, eq]))

cv2.waitKey()
plt.figure()
plt.title("Histogram")
plt.xlabel("# of bin")
plt.ylabel("Occurences")

bins = 32
hist = cv2.calcHist([image], [0], None, [bins], [0, 256])
plt.plot(hist)
histeq = cv2.calcHist([eq], [0], None, [bins], [0, 256])
plt.plot(histeq)

plt.xlim([0, bins])
plt.show()

