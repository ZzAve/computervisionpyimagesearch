import image_loader
import numpy as np
import cv2
from matplotlib import pyplot as plt

image = image_loader.load_image_from_args()



gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("Gray", gray)
cv2.waitKey(1000)
hist = cv2.calcHist([gray], [0], None, [256], [0, 256])

plt.figure()
plt.title("Grayscale historgram")
plt.xlabel("Pixel value")
plt.ylabel("Occurences")
plt.plot(hist)
plt.xlim([0,256])
plt.show()
cv2.waitKey()
