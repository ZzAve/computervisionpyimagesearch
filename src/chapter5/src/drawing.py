# USAGE
# python src/chapter4/src/getting_and_setting.py --image images/trex.png
import random

import numpy as np
import cv2

CANVAS_SIZE = (300, 300)

canvas = np.zeros((CANVAS_SIZE[1], CANVAS_SIZE[0], 3), dtype="uint8")

RED = (0, 0, 255)
GREEN = (0, 255, 0)
BLUE = (255, 0, 0)
WHITE = (255, 255, 255)

cv2.line(canvas, (0, 0), CANVAS_SIZE, GREEN)
cv2.imshow("Canvas", canvas)
# cv2.waitKey(0)


cv2.arrowedLine(canvas, (CANVAS_SIZE[0], 0), (0, CANVAS_SIZE[1]), RED, 3)
cv2.imshow("Canvas", canvas)
cv2.waitKey(0)

cv2.rectangle(canvas, (10, 10), (60, 60), GREEN)  # green rectangle
cv2.rectangle(canvas, (200, 50), (225, 125), BLUE, -1)  # blue filled rectangle
cv2.rectangle(canvas, (50, 200), (200, 225), RED, 3)  # fat open red rectangle
# cv2.rectangle(image, pt1(as x,y), pt2(as x,y), color, thickness)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)

circle_canvas = np.zeros((CANVAS_SIZE[1], CANVAS_SIZE[0], 3), dtype="uint8")
center = (circle_canvas.shape[1] // 2, circle_canvas.shape[0] // 2)

for radius in range(0, 150 + 1, 25):
    print("Radius: {}".format(radius))
    cv2.circle(circle_canvas, center, radius, WHITE, 1)

cv2.imshow("Circle Canvas", circle_canvas)
cv2.waitKey(0)

abstract_canvas = np.zeros((CANVAS_SIZE[1], CANVAS_SIZE[0], 3), dtype="uint8")
center = (circle_canvas.shape[1] // 2, circle_canvas.shape[0] // 2)

margin = 100
for i in range(0, 25):
    radius = np.random.randint(1, 150)
    center = np.random.randint(0 - margin, high=CANVAS_SIZE[1] + margin, size=(2,))
    color = np.random.randint(0, 256, size=(3,)).tolist()
    print("Random circle on: Center {}, Radius: {}, color: {}".format(center, radius, color))
    cv2.circle(abstract_canvas, tuple(center), radius, color, -1)

cv2.imshow("Abstract Circle Canvas", abstract_canvas)
cv2.waitKey(0)
