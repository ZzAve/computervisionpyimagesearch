import numpy as np
import cv2

# draw canvas
canvas = np.zeros((300, 300, 3), dtype="uint8")

# draw background
for x in range(0, 300, 10):
    for y in range(0, 300, 10):
        # even row (0, 2, 4):
        if x % 20 != y % 20:
            cv2.rectangle(canvas, (x, y), (x + 9, y + 9), (0, 0, 255), -1)

# draw cirle
cv2.circle(canvas, (150, 150), 50, (0, 255, 0), -1)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)
