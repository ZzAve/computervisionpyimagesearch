import numpy as np
import cv2


def translate(image, tx, ty):
    M = np.float32([
        [1, 0, tx],
        [0, 1, ty]])
    return cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))


def rotate(image, angle, center=None, scale=1.0):
    (h, w) = image.shape[:2]
    print('using me')
    if center is None:
        center = (w // 2, h // 2)

    M = cv2.getRotationMatrix2D(center, angle, scale)
    return cv2.warpAffine(image, M, (w, h))


def resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    if width is None and height is None:
        return image

    image_ratio = image.shape[0] / float(image.shape[1])
    if width is None:
        # derive width from height
        width = height / image_ratio
    elif height is None:
        # derive height from width
        height = width * image_ratio

    resized = cv2.resize(image, (int(width), int(height)), interpolation=inter)
    return resized
