import image_loader
import numpy as np
import cv2
from sklearn.cluster import MiniBatchKMeans

image = image_loader.load_image_from_args()
print("Printing original image")
cv2.waitKey()


def quantize_image(img, n_clusters=8):
    base = img.copy()
    (h, w, d) = base.shape

    # Convert to lab color space and create pixel vector
    base = base.reshape((h * w, d))

    # apply kmeans
    clt = MiniBatchKMeans(n_clusters=n_clusters)
    labels = clt.fit_predict(base)
    quant = clt.cluster_centers_.astype("uint8")[labels]

    # Reshape back to matrix
    quant = quant.reshape((h, w, d))
    # base = base.reshape((h, w, d))

    # cv2.imshow("Quant", quant)
    # cv2.imshow("base reshaped", base)
    # cv2.waitKey()

    # cv2.imshow("Quant BGR", quant)
    # cv2.imshow("base reshaped BGR", base)
    return quant


imageLab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
imageHsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

print("4")
quantized_imageLAB = quantize_image(imageLab, 4)
quantized_imageLAB = cv2.cvtColor(quantized_imageLAB, cv2.COLOR_LAB2BGR)
quantized_imageRGB = quantize_image(image, 4)
quantized_imageHSV = quantize_image(imageHsv, 4)
quantized_imageHSV = cv2.cvtColor(quantized_imageHSV, cv2.COLOR_HSV2BGR)
cv2.imshow("Quantized image (4) orig/ LAB / rgb / hsv", np.hstack([image, quantized_imageLAB, quantized_imageRGB, quantized_imageHSV]))


print("8")
quantized_imageLAB = quantize_image(imageLab, 8)
quantized_imageLAB = cv2.cvtColor(quantized_imageLAB, cv2.COLOR_LAB2BGR)
quantized_imageRGB = quantize_image(image, 8)
quantized_imageHSV = quantize_image(imageHsv, 8)
quantized_imageHSV = cv2.cvtColor(quantized_imageHSV, cv2.COLOR_HSV2BGR)
cv2.imshow("Quantized image (8) orig/ LAB / rgb / hsv", np.hstack([image, quantized_imageLAB, quantized_imageRGB, quantized_imageHSV]))


print("16")
quantized_imageLAB = quantize_image(imageLab, 16)
quantized_imageLAB = cv2.cvtColor(quantized_imageLAB, cv2.COLOR_LAB2BGR)
quantized_imageRGB = quantize_image(image, 16)
quantized_imageHSV = quantize_image(imageHsv, 16)
quantized_imageHSV = cv2.cvtColor(quantized_imageHSV, cv2.COLOR_HSV2BGR)
cv2.imshow("Quantized image (16) orig/ LAB / rgb / hsv", np.hstack([image, quantized_imageLAB, quantized_imageRGB, quantized_imageHSV]))


cv2.waitKey()
