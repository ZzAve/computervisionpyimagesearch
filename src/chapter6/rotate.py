import numpy as np
import argparse
import random
import imutils
import cv2

ap = argparse.ArgumentParser();
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"]);
cv2.imshow("Original", image);

print("image size: {}".format(image.shape))

(h, w) = image.shape[:2]
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, 45, 1.0)

rotated = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Rotated positively 45 deg", rotated)

M = cv2.getRotationMatrix2D(center, -90, 1.0)
rotated = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Rotated negatively 90 deg", rotated)

angle = np.random.randint(-360, 360)
rotated = imutils.rotate(image, angle)
cv2.imshow("Rotated (fancy) random degrees", rotated)

cv2.waitKey(0)
