import numpy as np
import argparse
import imutils
import cv2

ap = argparse.ArgumentParser();
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"]);
cv2.imshow("Original", image);

print("image size: {}".format(image.shape))
M = np.float32([
    [0, 1, 0],
    [1, 0, 0]])
print(M)

shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted down and right", shifted)


# cv2.waitKey(0)

M = np.float32([
    [1, 0, -50],
    [0, 1, -90]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted Up and Left", shifted)

# cv2.waitKey(0)

M = np.float32([
    [-1, 0, image.shape[1]],
    [0, -1, image.shape[0]]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Mirrored", shifted)


shifted = imutils.translate(image, 0, 100)
cv2.imshow("Translated, imutils", shifted)
cv2.waitKey(0)




