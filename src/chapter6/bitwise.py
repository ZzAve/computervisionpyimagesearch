import numpy as np
import cv2

rectangle = np.zeros((300, 300), dtype="uint8")
cv2.rectangle(rectangle, (25, 25), (275, 275), 255, -1)

cv2.imshow("Rectangle", rectangle)

circle = np.zeros((300, 300), dtype="uint8")
cv2.circle(circle, (150, 150), 150, 255, -1)
cv2.imshow("Circle", circle)

cv2.waitKey()

bitwiseAnd = cv2.bitwise_and(rectangle, circle)
bitwiseOr = cv2.bitwise_or(rectangle, circle)
bitwiseXor = cv2.bitwise_xor(rectangle, circle)
bitwsieNot = cv2.bitwise_not(circle)

cv2.imshow("bitwise AND", bitwiseAnd)
cv2.imshow("bitwise OR", bitwiseOr)
cv2.imshow("bitwise XOR", bitwiseXor)
cv2.imshow("bitwise NOT", bitwsieNot)

cv2.waitKey()
