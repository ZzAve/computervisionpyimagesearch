import numpy as np
import cv2
import argparse
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original image", image)


cropped = image[30:120, 240:335]
cv2.imshow("Cropped image", cropped)

cv2.waitKey(0)
