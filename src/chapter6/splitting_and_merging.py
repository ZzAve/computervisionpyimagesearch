import numpy as np
import cv2
import image_loader

image = image_loader.load_image_from_args()

(B, G, R) = cv2.split(image)

cv2.imshow("Red", R)
cv2.imshow("Green", G)
cv2.imshow("Blue", B)

merged = cv2.merge([B, G, R])
mergedWrong = cv2.merge([R, G, B])
cv2.imshow("Merged", merged)
cv2.imshow("Merged wrong", mergedWrong)

cv2.waitKey()

zeros = np.zeros(image.shape[:2], dtype="uint8")
# Merged in red
mergedRed = cv2.merge([zeros, zeros, R])
mergedGreen = cv2.merge([zeros, G,  zeros])
mergedBlue = cv2.merge([B, zeros, zeros])

cv2.imshow("Red", mergedRed)
cv2.imshow("Green", mergedGreen)
cv2.imshow("Blue", mergedBlue)

cv2.waitKey()
cv2.destroyAllWindows()
