import numpy as np
import cv2
import argparse
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original image", image)

# Resize by width spec

r = 66.0 / image.shape[1]
dim = (66, int(image.shape[0] * r))
resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

print(resized.shape)
cv2.imshow("Resized image (150px width)", resized)

# Resize by height spec


r = 110.0 / image.shape[0]
dim = (int(image.shape[1] * r), 110)
resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

cv2.imshow("Resized image (110px height)", resized)
print("100px height image: ", resized.shape)

# Resize by scale
dim = (int(image.shape[1] * 0.8), int(image.shape[0] * 0.8))
resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

cv2.imshow("Resized image (0.8x original width / height)", resized)

# Lib function
resized = imutils.resize(image, width=100)
cv2.imshow("W Resized image (100 px width)", resized)

resized = imutils.resize(image, height=100)
cv2.imshow("H Resized image (50 px height)", resized)

resized = imutils.resize(image, height=100, width=200)
cv2.imshow("Weird", resized)

cv2.waitKey(0)
