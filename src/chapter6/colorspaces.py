import image_loader
import numpy as np
import cv2

image = image_loader.load_image_from_args();
cv2.waitKey()

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

cv2.imshow("Gray", gray)
cv2.imshow("HSV", hsv)
cv2.imshow("Lab", lab)

cv2.waitKey()
