import numpy as np
import cv2
import argparse
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original image", image)

print("max of 255: {}".format(cv2.add(np.uint8([200]), np.uint8([100]))))
print("min of 0: {}".format(cv2.subtract(np.uint8([10]), np.uint8([20]))))

print("wraparound of 255: {}".format(np.uint8([200]) + np.uint8([100])))
print("wraparound of 0: {}".format(np.uint8([10]) - np.uint8([20])))

M = np.ones(image.shape, dtype="uint8") * 100
added = cv2.add(image, M)
cv2.imshow("Added", added)

added2 = image + M
cv2.imshow("Added wraparound", added2)

M = np.ones(image.shape, dtype="uint8") * 50
subtracted = cv2.subtract(image, M)

cv2.imshow("Subtracted", subtracted)

subtracted2 = image - M
cv2.imshow("Subtracted wraparound", subtracted2)

cv2.waitKey(0)
