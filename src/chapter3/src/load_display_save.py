# USAGE
# python load_display_save.py --image ../images/trex.png

# Import the necessary packages
from __future__ import print_function
import argparse
import cv2
import matplotlib.pyplot as plt



plt.plot([1, 2, 3, 4])
plt.ylabel('some numbers')
plt.show()

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                help="Path to the image")
args = vars(ap.parse_args())

print(args);

# Load the image and show some basic information on it
image = cv2.imread(args["image"])
print("width: {} pixels".format(image.shape[1]))
print("height: {} pixels".format(image.shape[0]))
print("channels: {}".format(image.shape[2]))

# Show the image and wait for a keypress
imgplot = plt.imshow(image)

cv2.imshow("Image", image)



lum_img = image[:, :, 0]


fig = plt.figure()
a = fig.add_subplot(1, 2, 1)
imgplot = plt.imshow(lum_img)
a.set_title('Before')
plt.colorbar(ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal')
a = fig.add_subplot(1, 2, 2)
imgplot = plt.imshow(lum_img, cmap="hot")
a.set_title('After')
plt.colorbar(ticks=[0.1, 0.3, 0.5, 0.7], orientation='horizontal')
plt.show()

cv2.waitKey(0)


# Save the image -- OpenCV handles converting filetypes
# automatically
cv2.imwrite("newimage.jpg", image)
